FROM 	ubuntu:18.04
RUN 	apt update -y && apt install mysql-server-5.7 -y
RUN     apt-get install mysql-client -y
COPY 	department.sql .
COPY 	run.sh .
RUN 	chmod +x /run.sh
CMD 	["/run.sh"]